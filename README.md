# SPF: Social Proximity Framework #

## SPF Library ##
This project contains the library of the Social Proximity Framework. It aims at easing the development of social proximity applications. In detail, the library provides the access to SPF functionalities and hides the details of the interaction with the framework. For more information about SPF see: https://bitbucket.org/dariodotcom/spfapp/

SPF is a project developed at 
Dipartimento di Elettronica Informazione e Bioingegneria
Politecnico di Milano, Milan, Italy