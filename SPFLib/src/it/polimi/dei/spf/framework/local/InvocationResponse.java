/* 
 * Copyright 2014 Jacopo Aliprandi, Dario Archetti
 * 
 * This file is part of SPF.
 * 
 * SPF is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * SPF is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with SPF.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package it.polimi.dei.spf.framework.local;

import android.os.Parcel;
import android.os.Parcelable;

// TODO: Auto-generated Javadoc
/**
 * The Class InvocationResponse.
 */
public class InvocationResponse implements Parcelable {

	/** The Constant RESULT. */
	public final static int RESULT = 0;
	
	/** The Constant ERROR. */
	public final static int ERROR = 1;

	/** The type. */
	private final int type;
	
	/** The error message. */
	private String errorMessage;
	
	/** The result. */
	private Object result;

	/**
	 * Result.
	 *
	 * @param result the result
	 * @return the invocation response
	 */
	public static InvocationResponse result(Object result) {
		InvocationResponse resp = new InvocationResponse(RESULT);
		resp.result = result;
		return resp;
	}

	/**
	 * Error.
	 *
	 * @param t the t
	 * @return the invocation response
	 */
	public static InvocationResponse error(Throwable t) {
		InvocationResponse resp = new InvocationResponse(ERROR);
		resp.errorMessage = t.getClass().getName() + ":" + t.getLocalizedMessage();
		return resp;
	}

	/**
	 * Error.
	 *
	 * @param errorMessage the error message
	 * @return the invocation response
	 */
	public static InvocationResponse error(String errorMessage) {
		InvocationResponse resp = new InvocationResponse(ERROR);
		resp.errorMessage = errorMessage;
		return resp;
	}

	/**
	 * Instantiates a new invocation response.
	 *
	 * @param type the type
	 */
	private InvocationResponse(int type) {
		this.type = type;
	}

	/**
	 * Instantiates a new invocation response.
	 *
	 * @param source the source
	 */
	private InvocationResponse(Parcel source) {
		type = source.readInt();
		switch (type) {
		case ERROR:
			errorMessage = source.readString();
			break;
		case RESULT:
			result = source.readValue(null);
			break;
		}
	}

	/**
	 * Gets the error message.
	 *
	 * @return the error message
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * Sets the error message.
	 *
	 * @param errorMessage the new error message
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	/**
	 * Gets the result.
	 *
	 * @return the result
	 */
	public Object getResult() {
		return result;
	}

	/**
	 * Sets the result.
	 *
	 * @param result the new result
	 */
	public void setResult(Object result) {
		this.result = result;
	}

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public int getType() {
		return type;
	}

	/**
	 * Checks if is result.
	 *
	 * @return true, if is result
	 */
	public boolean isResult() {
		return type == RESULT;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Invocation response: " + (type == RESULT ? "RESULT\n" + result : "ERROR\n" + errorMessage);
	}

	/* (non-Javadoc)
	 * @see android.os.Parcelable#describeContents()
	 */
	@Override
	public int describeContents() {
		return 0;
	}

	/* (non-Javadoc)
	 * @see android.os.Parcelable#writeToParcel(android.os.Parcel, int)
	 */
	@Override
	public void writeToParcel(Parcel dest, int flag) {
		dest.writeInt(type);
		switch (type) {
		case ERROR:
			dest.writeString(errorMessage);
			break;
		case RESULT:
			dest.writeValue(result);
			break;
		}
	}

	/** The Constant CREATOR. */
	public final static Parcelable.Creator<InvocationResponse> CREATOR = new Creator<InvocationResponse>() {

		@Override
		public InvocationResponse[] newArray(int arg0) {
			return new InvocationResponse[arg0];
		}

		@Override
		public InvocationResponse createFromParcel(Parcel arg0) {
			return new InvocationResponse(arg0);
		}
	};
}
