/* 
 * Copyright 2014 Jacopo Aliprandi, Dario Archetti
 * 
 * This file is part of SPF.
 * 
 * SPF is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * SPF is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with SPF.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package it.polimi.dei.spf.framework.local;

// TODO: Auto-generated Javadoc
/**
 * The Enum Permission.
 */
public enum Permission {

	/** The register services. */
	REGISTER_SERVICES(0x1),
	
	/** The search service. */
	SEARCH_SERVICE(0x2),
	
	/** The execute local services. */
	EXECUTE_LOCAL_SERVICES(0x4),
	
	/** The execute remote services. */
	EXECUTE_REMOTE_SERVICES(0x8),
	
	/** The read local profile. */
	READ_LOCAL_PROFILE(0x10),
	
	/** The read remote profiles. */
	READ_REMOTE_PROFILES(0x20),
	
	/** The write local profile. */
	WRITE_LOCAL_PROFILE(0x40),
	
	/** The notification services. */
	NOTIFICATION_SERVICES(0x80),
	
	/** The activity service. */
	ACTIVITY_SERVICE(0x100);
	
	/** The m code. */
	private int mCode;
	
	/**
	 * Instantiates a new permission.
	 *
	 * @param code the code
	 */
	private Permission(int code) {
		this.mCode = code;
	}
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public int getCode(){
		return mCode;
	}
	
}
