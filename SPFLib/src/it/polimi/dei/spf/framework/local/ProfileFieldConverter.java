/* 
 * Copyright 2014 Jacopo Aliprandi, Dario Archetti
 * 
 * This file is part of SPF.
 * 
 * SPF is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * SPF is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with SPF.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package it.polimi.dei.spf.framework.local;

import java.io.ByteArrayOutputStream;
import java.lang.reflect.Array;
import java.util.Date;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

// TODO: Auto-generated Javadoc
/**
 * The Class ProfileFieldConverter.
 *
 * @param <E> the element type
 */
public abstract class ProfileFieldConverter<E extends Object> {

	/** The Constant STRING_CONVERTER. */
	private static final ProfileFieldConverter<String> STRING_CONVERTER = new StringConverter();
	
	/** The Constant DATE_CONVERTER. */
	private static final ProfileFieldConverter<Date> DATE_CONVERTER = new DateConverter();
	
	/** The Constant STRING_ARRAY_CONVERTER. */
	private static final ProfileFieldConverter<String[]> STRING_ARRAY_CONVERTER = new ArrayConverter<String>(String.class, STRING_CONVERTER);
	
	/** The Constant BITMAP_CONVERTER. */
	private static final ProfileFieldConverter<Bitmap> BITMAP_CONVERTER = new BitmapConverter();
	
	/** The Constant ARRAY_SEPARATOR. */
	private static final String ARRAY_SEPARATOR = ";";

	/**
	 * For field.
	 *
	 * @param <E> the element type
	 * @param field the field
	 * @return the profile field converter
	 */
	@SuppressWarnings("unchecked")
	public static <E> ProfileFieldConverter<E> forField(ProfileField<E> field) {
		Class<?> fieldClass = field.getFieldClass();

		if (fieldClass == String.class) {
			return (ProfileFieldConverter<E>) STRING_CONVERTER;
		} else if (fieldClass == Date.class) {
			return (ProfileFieldConverter<E>) DATE_CONVERTER;
		} else if (fieldClass == String[].class) {
			return (ProfileFieldConverter<E>) STRING_ARRAY_CONVERTER;
		} else if (fieldClass == Bitmap.class) {
			return (ProfileFieldConverter<E>) BITMAP_CONVERTER;
		}

		throw new IllegalArgumentException("Field type " + fieldClass.getSimpleName() + " not supported");
	}

	// public methods
	/**
	 * To storage string.
	 *
	 * @param value the value
	 * @return the string
	 */
	public abstract String toStorageString(E value);

	/**
	 * From storage string.
	 *
	 * @param storageString the storage string
	 * @return the e
	 */
	public abstract E fromStorageString(String storageString);

	// Implementations
	/**
	 * The Class StringConverter.
	 */
	private static class StringConverter extends ProfileFieldConverter<String> {

		/* (non-Javadoc)
		 * @see it.polimi.dei.spf.framework.local.ProfileFieldConverter#toStorageString(java.lang.Object)
		 */
		@Override
		public String toStorageString(String value) {
			return value;
		}

		/* (non-Javadoc)
		 * @see it.polimi.dei.spf.framework.local.ProfileFieldConverter#fromStorageString(java.lang.String)
		 */
		@Override
		public String fromStorageString(String storageString) {
			return storageString;
		}

	}

	/**
	 * The Class DateConverter.
	 */
	private static class DateConverter extends ProfileFieldConverter<Date> {

		/* (non-Javadoc)
		 * @see it.polimi.dei.spf.framework.local.ProfileFieldConverter#toStorageString(java.lang.Object)
		 */
		@Override
		public String toStorageString(Date value) {
			return String.valueOf(value.getTime());
		}

		/* (non-Javadoc)
		 * @see it.polimi.dei.spf.framework.local.ProfileFieldConverter#fromStorageString(java.lang.String)
		 */
		@Override
		public Date fromStorageString(String storageString) {
			return new Date(Long.valueOf(storageString));
		}
	}

	/**
	 * The Class ArrayConverter.
	 *
	 * @param <E> the element type
	 */
	private static class ArrayConverter<E> extends ProfileFieldConverter<E[]> {

		/** The m component type. */
		private Class<E> mComponentType;
		
		/** The m base converter. */
		private ProfileFieldConverter<E> mBaseConverter;

		/**
		 * Instantiates a new array converter.
		 *
		 * @param componentType the component type
		 * @param baseConverter the base converter
		 */
		public ArrayConverter(Class<E> componentType, ProfileFieldConverter<E> baseConverter) {
			this.mComponentType = componentType;
			this.mBaseConverter = baseConverter;
		}

		/* (non-Javadoc)
		 * @see it.polimi.dei.spf.framework.local.ProfileFieldConverter#toStorageString(java.lang.Object)
		 */
		@Override
		public String toStorageString(E[] value) {
			StringBuilder b = new StringBuilder();
			for (int i = 0; i < value.length; i++) {
				b.append(mBaseConverter.toStorageString(value[i]));
				if ((i + 1) < value.length) {
					b.append(ARRAY_SEPARATOR);
				}
			}
			return b.toString();
		}

		/* (non-Javadoc)
		 * @see it.polimi.dei.spf.framework.local.ProfileFieldConverter#fromStorageString(java.lang.String)
		 */
		@SuppressWarnings("unchecked")
		@Override
		public E[] fromStorageString(String storageString) {
			String[] values = storageString.split(ARRAY_SEPARATOR);
			Object array = Array.newInstance(mComponentType, values.length);

			for (int i = 0; i < values.length; i++) {
				Array.set(array, i, mBaseConverter.fromStorageString(values[i]));
			}

			return (E[]) array;
		}

	}

	/**
	 * The Class BitmapConverter.
	 */
	private static class BitmapConverter extends ProfileFieldConverter<Bitmap> {

		/* (non-Javadoc)
		 * @see it.polimi.dei.spf.framework.local.ProfileFieldConverter#toStorageString(java.lang.Object)
		 */
		@Override
		public String toStorageString(Bitmap value) {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			value.compress(Bitmap.CompressFormat.JPEG, 100, baos);
			return Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT);
		}

		/* (non-Javadoc)
		 * @see it.polimi.dei.spf.framework.local.ProfileFieldConverter#fromStorageString(java.lang.String)
		 */
		@Override
		public Bitmap fromStorageString(String storageString) {
			byte[] ba = Base64.decode(storageString.getBytes(), Base64.DEFAULT);
			Bitmap photo = BitmapFactory.decodeByteArray(ba, 0, ba.length);
			return photo;
		}
	}
}
