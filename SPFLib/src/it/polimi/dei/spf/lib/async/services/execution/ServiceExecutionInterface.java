/* 
 * Copyright 2014 Jacopo Aliprandi, Dario Archetti
 * 
 * This file is part of SPF.
 * 
 * SPF is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * SPF is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with SPF.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package it.polimi.dei.spf.lib.async.services.execution;

import it.polimi.dei.spf.framework.local.InvocationRequest;
import it.polimi.dei.spf.framework.local.InvocationResponse;
import it.polimi.dei.spf.framework.local.SPFActivity;

/**
 * Interface for components that provide low level functionalities for executing
 * remote service.
 * 
 * @author darioarchetti
 * 
 */
public interface ServiceExecutionInterface {

	/**
	 * Dispatches an {@link InvocationRequest} to a given target and provides
	 * back the outcome, either a result or an exception, of the remote
	 * execution wrapped in an {@link InvocationResponse}.
	 * 
	 * @param target
	 *            - the identifier of the target to which dispatch the
	 *            invocation request.
	 * @param request
	 *            - the invocation request
	 * @return the response containing the outcome of the invocation.
	 */
	public InvocationResponse executeService(String target, InvocationRequest request);

	/**
	 * Dispatches an activity to the SPF instances matching the given
	 * identifier. The framework will perform information injection into the
	 * activity: such information will be available to the caller once the
	 * method ends.
	 * 
	 * @param target
	 *            - the target of the activity.
	 * @param activity
	 *            - the activity to dispatch.
	 * @return - true if the activity has been correctly consumed by the
	 *         recipient.
	 */
	public boolean sendActivity(String target, SPFActivity activity);
}
