/* 
 * Copyright 2014 Jacopo Aliprandi, Dario Archetti
 * 
 * This file is part of SPF.
 * 
 * SPF is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * SPF is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with SPF.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package it.polimi.dei.spf.framework.local;

import it.polimi.dei.spf.framework.local.InvocationResponse;
import it.polimi.dei.spf.framework.local.InvocationRequest;
import it.polimi.dei.spf.framework.local.ServiceDescriptor;
import it.polimi.dei.spf.framework.local.AppDescriptor;
import it.polimi.dei.spf.framework.local.SearchDescriptor;
import it.polimi.dei.spf.framework.local.SPFSearchCallback;
import it.polimi.dei.spf.framework.local.ProfileFieldContainer;
import it.polimi.dei.spf.framework.local.SPFError;
import it.polimi.dei.spf.framework.local.SPFActivity;

/*
 * Services that allows the interaction with remote
 * instances of SPF
 */
interface SPFProximityService {
  
    // Method execution
    InvocationResponse executeRemoteService(String accessToken, String target, in InvocationRequest request, out SPFError err);
    InvocationResponse sendActivity(String accessToken, String target, in SPFActivity activity, out SPFError err);
    void injectInformationIntoActivity(String accessToken, String target, inout SPFActivity activity, out SPFError error);
    
    // Profile information retrieval
    ProfileFieldContainer getProfileBulk(String accessToken, String target, in String[] fieldIdentifiers, out SPFError err);
    
    // Search for people
    void registerCallback(in String accessToken, SPFSearchCallback callback, out SPFError err);
    boolean unregisterCallback(in String accessToken, out SPFError err);
    String startNewSearch(in String accessToken, in SearchDescriptor searchDescriptor, out SPFError err);
    void stopSearch(in String accessToken, in String queryId, out SPFError err);
    boolean lookup(in String accessToken, String personIdentifier, out SPFError err);
}