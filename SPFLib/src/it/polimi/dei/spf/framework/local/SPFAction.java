/* 
 * Copyright 2014 Jacopo Aliprandi, Dario Archetti
 * 
 * This file is part of SPF.
 * 
 * SPF is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * SPF is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with SPF.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package it.polimi.dei.spf.framework.local;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcelable;

// TODO: Auto-generated Javadoc
/**
 * The Class SPFAction.
 *
 * @author Jacopo Aliprandi //TODO comment
 */
public abstract class SPFAction implements Parcelable {

	/** The Constant TYPE_NOTIFICATION. */
	protected static final int TYPE_NOTIFICATION = 0;
	
	/** The Constant TYPE_INTENT. */
	protected static final int TYPE_INTENT = 1;
	
	/** The Constant TYPE_SERVICE_CALL. */
	protected static final int TYPE_SERVICE_CALL = 2;

	/** The Constant KEY_TYPE. */
	protected static final String KEY_TYPE = "type";

	/**
	 * TODO comment.
	 *
	 * @return the string
	 */
	public abstract String toJSON();

	/**
	 * TODO comment.
	 *
	 * @param json the json
	 * @return the SPF action
	 */
	public static SPFAction fromJSON(String json) {
		try {
			JSONObject o = new JSONObject(json);
			int type = o.getInt(KEY_TYPE);
			switch (type) {
			case TYPE_INTENT:
				return new SPFActionIntent(o);
			case TYPE_NOTIFICATION:
				return new SPFActionSendNotification(o);
			default:
				throw new IllegalArgumentException("Unknown action type: " + type);
			}

		} catch (JSONException e) {
			throw new IllegalArgumentException("JSON does not represent a valid SPFAction");
		}
	}

}
