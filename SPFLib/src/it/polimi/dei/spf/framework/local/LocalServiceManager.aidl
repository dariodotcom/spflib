/* 
 * Copyright 2014 Jacopo Aliprandi, Dario Archetti
 * 
 * This file is part of SPF.
 * 
 * SPF is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * SPF is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with SPF.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package it.polimi.dei.spf.framework.local;

import it.polimi.dei.spf.framework.local.AppDescriptor;
import it.polimi.dei.spf.framework.local.ServiceDescriptor;
import it.polimi.dei.spf.framework.local.InvocationRequest;
import it.polimi.dei.spf.framework.local.InvocationResponse;
import it.polimi.dei.spf.framework.local.SPFError;
import it.polimi.dei.spf.framework.local.SPFActivity;

/**
 * Service that allows applications to register services
 * to be made available for execution, and to execute the
 * services of local applications.
 */
interface LocalServiceManager {
	InvocationResponse sendActivityLocally(String accessToken, in SPFActivity activity, out SPFError error);
    InvocationResponse executeLocalService(String accessToken, in InvocationRequest request, out SPFError error);
    boolean registerService(String accessToken, in ServiceDescriptor descriptor, out SPFError error);
    boolean unregisterService(String accessToken, in ServiceDescriptor descriptor, out SPFError error);
    void injectInformationIntoActivity(String accessToken, inout SPFActivity activity, out SPFError error);
}