/* 
 * Copyright 2014 Jacopo Aliprandi, Dario Archetti
 * 
 * This file is part of SPF.
 * 
 * SPF is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * SPF is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with SPF.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package it.polimi.dei.spf.framework.local;

import android.os.Parcel;
import android.os.Parcelable;

// TODO: Auto-generated Javadoc
/**
 * TODO SECURITY REFACTOR - reuse in app registration
 * remove intent
 * add:
 *  name
 *  permission (later)
 *  app icon (feasible?).
 *
 * @author darioarchetti
 */
public class AppDescriptor implements Parcelable {

	/** The m app identifier. */
	private String mAppIdentifier;
	
	/** The m app name. */
	private String mAppName;
	
	/** The m version. */
	private String mVersion;
	
	/** The m permission codes. */
	private int mPermissionCodes;

	/**
	 * Instantiates a new app descriptor.
	 *
	 * @param appIdentifier the app identifier
	 * @param appName the app name
	 * @param version the version
	 * @param permissionCodes the permission codes
	 */
	public AppDescriptor(String appIdentifier, String appName, String version, int permissionCodes) {
		this.mAppIdentifier = appIdentifier;
		this.mAppName = appName;
		this.mVersion = version;
		this.mPermissionCodes = permissionCodes;
	}

	/**
	 * Instantiates a new app descriptor.
	 *
	 * @param source the source
	 */
	private AppDescriptor(Parcel source) {
		this.mAppIdentifier = source.readString();
		this.mAppName = source.readString();
		this.mVersion = source.readString();
		this.mPermissionCodes = source.readInt();
	}

	/**
	 * Gets the app identifier.
	 *
	 * @return the app identifier
	 */
	public String getAppIdentifier() {
		return mAppIdentifier;
	}

	/**
	 * Gets the app name.
	 *
	 * @return the app name
	 */
	public String getAppName() {
		return mAppName;
	}

	/**
	 * Gets the version.
	 *
	 * @return the version
	 */
	public String getVersion() {
		return mVersion;
	}

	/**
	 * Gets the permission code.
	 *
	 * @return the permission code
	 */
	public int getPermissionCode() {
		return mPermissionCodes;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return mAppIdentifier + "-" + mVersion;
	}
	
	/* (non-Javadoc)
	 * @see android.os.Parcelable#describeContents()
	 */
	@Override
	public int describeContents() {
		return 0;
	}

	/* (non-Javadoc)
	 * @see android.os.Parcelable#writeToParcel(android.os.Parcel, int)
	 */
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(mAppIdentifier);
		dest.writeString(mAppName);
		dest.writeString(mVersion);
		dest.writeInt(mPermissionCodes);
	}

	/** The Constant CREATOR. */
	public final static Parcelable.Creator<AppDescriptor> CREATOR = new Creator<AppDescriptor>() {

		@Override
		public AppDescriptor[] newArray(int size) {
			return new AppDescriptor[size];
		}

		@Override
		public AppDescriptor createFromParcel(Parcel source) {
			return new AppDescriptor(source);
		}
	};

}
