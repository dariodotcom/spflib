/* 
 * Copyright 2014 Jacopo Aliprandi, Dario Archetti
 * 
 * This file is part of SPF.
 * 
 * SPF is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * SPF is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with SPF.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package it.polimi.dei.spf.lib.async.services.execution;

import it.polimi.dei.spf.framework.local.InvocationRequest;
import it.polimi.dei.spf.framework.local.InvocationResponse;
import it.polimi.dei.spf.framework.local.LocalServiceManager;
import it.polimi.dei.spf.framework.local.SPFActivity;
import it.polimi.dei.spf.framework.local.SPFError;
import it.polimi.dei.spf.framework.local.ServiceDescriptor;
import it.polimi.dei.spf.lib.async.AccessTokenManager;
import it.polimi.dei.spf.lib.async.LocalComponent;
import it.polimi.dei.spf.lib.async.Person;
import it.polimi.dei.spf.lib.async.services.ServiceInterface;
import it.polimi.dei.spf.lib.async.services.ServiceInvocationException;

import android.content.Context;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;

/**
 * SPF component to create invocation stubs to invoke remote methods.
 * 
 * @author darioarchetti
 * 
 */
public final class LocalServiceExecutor extends LocalComponent<LocalServiceManager> {

	private static final String SERVICE_INTENT = "it.polimi.dei.spf.services.LocalServiceExecutor";
	private static final LocalComponent.Descriptor<LocalServiceManager> DESCRIPTOR = new LocalComponent.Descriptor<LocalServiceManager>() {

		@Override
		public String getIntentName() {
			return SERVICE_INTENT;
		}

		@Override
		public LocalServiceManager castInterface(IBinder binder) {
			return LocalServiceManager.Stub.asInterface(binder);
		}

		@Override
		public LocalComponent<LocalServiceManager> createInstance(Context context, LocalServiceManager serviceInterface, ServiceConnection connection, BaseCallback<LocalServiceManager> callback) {
			return new LocalServiceExecutor(context, serviceInterface, connection, callback);
		}
	};

	public static void load(Context context, final Callback callback) {
		LocalComponent.load(context, DESCRIPTOR, asBase(callback));
	}

	protected LocalServiceExecutor(Context context, LocalServiceManager serviceInterface, ServiceConnection connection, BaseCallback<LocalServiceManager> callback) {
		super(context, serviceInterface, connection, callback);
	}

	private InvocationTarget mLocalInvocationTarget = new InvocationTarget() {

		@Override
		public InvocationResponse executeService(InvocationRequest request) throws ServiceInvocationException {
			String token = AccessTokenManager.get(getContext()).getAccessToken();
			try {
				SPFError error = new SPFError();
				InvocationResponse response = getService().executeLocalService(token, request, error);

				if (error.isOk()) {
					return response;
				} else {
					handleError(error);
					throw new ServiceInvocationException(error.getMessage());
				}
			} catch (RemoteException e) {
				catchRemoteException(e);
				throw new ServiceInvocationException(e.getClass().getSimpleName());
			}

		}
	};

	/**
	 * Creates an invocation stub to send service invocation requests to the
	 * local person performing method calls. The stub is created from a provided
	 * service interface, which must be annotated with {@link ServiceInterface}
	 * describing the service. The method returns an object implementing the
	 * aforementioned interface that can be used to perform method invocation.
	 * 
	 * @param serviceInterface
	 *            - the interface of the service.
	 * @param classLoader
	 *            - the ClassLoader to load classes.
	 * @return an invocation stub to perform method calls.
	 */
	public <E> E createInvocationStub(Class<E> serviceInterface, ClassLoader classLoader) {
		return InvocationStub.from(serviceInterface, classLoader, mLocalInvocationTarget);
	}

	/**
	 * Creates an invocation stub to send service invocation requests to a
	 * target {@link Person} providing the name and the parameter list. The
	 * object is created from a {@link ServiceDescriptor} containing the
	 * required details.
	 * 
	 * @param target
	 *            - the person who the service invocation requests will be
	 *            dispatched to.
	 * @param descriptor
	 *            - the {@link ServiceDescriptor} of the service whose methods
	 *            to invoke.
	 * @return a {@link ServiceInvocationStub} to perform invocations of remote
	 *         services.
	 */
	public InvocationStub createInvocationStub(Person target, ServiceDescriptor descriptor) {
		return InvocationStub.from(descriptor, mLocalInvocationTarget);
	}

	/**
	 * Dispatches an activity to the local SPF instance. The framework will
	 * perform information injection into the activity: such information will be
	 * available to the caller once the method ends.
	 * 
	 * @param activity
	 *            - the activity to dispatch.
	 * @return - true if the activity has been correctly consumed.
	 */
	public boolean sendActivityLocally(SPFActivity activity) {
		String token = getAccessToken();
		SPFError err = new SPFError();
		InvocationResponse resp;

		try {
			getService().injectInformationIntoActivity(token, activity, err);
			if (!err.isOk()) {
				handleError(err);
				return false;
			}

			resp = getService().sendActivityLocally(token, activity, err);
		} catch (RemoteException e) {
			// TODO Error Management
			return false;
		}

		if (!err.isOk()) {
			handleError(err);
			return false;
		}

		if (!resp.isResult()) {
			return false;
		}

		return (Boolean) resp.getResult();
	}

	// We do not let Callback implement BaseCallback, otherwise apllications
	// would need to cast LocalComponent<> to the actual implementation.
	public interface Callback {
		public void onServiceReady(LocalServiceExecutor service);

		public void onError(SPFError errorMsg);

		public void onDisconnect();
	}

	private static BaseCallback<LocalServiceManager> asBase(final Callback callback) {
		return new BaseCallback<LocalServiceManager>() {

			@Override
			public void onServiceReady(LocalComponent<LocalServiceManager> serviceInterface) {
				callback.onServiceReady((LocalServiceExecutor) serviceInterface);
			}

			@Override
			public void onError(SPFError errorMsg) {
				callback.onError(errorMsg);
			}

			@Override
			public void onDisconnect() {
				callback.onDisconnect();
			}
		};
	}

	private void catchRemoteException(RemoteException e) {
		disconnect();
		getCallback().onDisconnect();
	}
}