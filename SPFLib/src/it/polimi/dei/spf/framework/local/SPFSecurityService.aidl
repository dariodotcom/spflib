/* 
 * Copyright 2014 Jacopo Aliprandi, Dario Archetti
 * 
 * This file is part of SPF.
 * 
 * SPF is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * SPF is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with SPF.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package it.polimi.dei.spf.framework.local;

import it.polimi.dei.spf.framework.local.AppDescriptor;
import it.polimi.dei.spf.framework.local.SPFAppRegistrationCallback;

/*
 * Service that allows applications to register themselves
 * and obtain an access token to perform requests to other
 * services of the framework.
 */
interface SPFSecurityService {
	void registerApp(in AppDescriptor descriptor, SPFAppRegistrationCallback callback);
	void unregisterApp(String accessToken);
}