/* 
 * Copyright 2014 Jacopo Aliprandi, Dario Archetti
 * 
 * This file is part of SPF.
 * 
 * SPF is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * SPF is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with SPF.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package it.polimi.dei.spf.lib.async.profile;

import it.polimi.dei.spf.framework.local.LocalProfileService;
import it.polimi.dei.spf.framework.local.ProfileField;
import it.polimi.dei.spf.framework.local.ProfileFieldContainer;
import it.polimi.dei.spf.framework.local.SPFError;
import it.polimi.dei.spf.lib.async.LocalComponent;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

/**
 * Local component that provides access to the local user profile, allowing read
 * and write operations.
 * 
 * @author darioarchetti
 * 
 */
public class LocalProfile extends LocalComponent<LocalProfileService> {

	private static final String INTENT_NAME = "it.polimi.dei.spf.framework.appservice.SPFProfileService";
	private static final String TAG = "LocalProfile";
	private static final LocalComponent.Descriptor<LocalProfileService> DESCRIPTOR = new LocalComponent.Descriptor<LocalProfileService>() {

		@Override
		public String getIntentName() {
			return INTENT_NAME;
		}

		@Override
		public LocalProfileService castInterface(IBinder binder) {
			return LocalProfileService.Stub.asInterface(binder);
		}

		@Override
		public LocalComponent<LocalProfileService> createInstance(Context context, LocalProfileService serviceInterface, ServiceConnection connection, it.polimi.dei.spf.lib.async.LocalComponent.BaseCallback<LocalProfileService> callback) {
			return new LocalProfile(context, serviceInterface, connection, callback);
		}

	};

	/**
	 * Loads the local profile connection asynchronously. The operations to
	 * perform once the connection is ready should be placed into an
	 * implementation of the {@link LocalProfile.Callback} interface.
	 * 
	 * @param context
	 *            - the Android {@link Context} to use.
	 * @param callback
	 *            - the callback to execute when the connection is loaded or an
	 *            error occurs.
	 */
	public static void load(final Context context, final Callback callback) {
		LocalComponent.load(context, DESCRIPTOR, asBase(callback));
	}

	private LocalProfile(Context context, LocalProfileService serviceInterface, ServiceConnection connection, BaseCallback<LocalProfileService> callback) {
		super(context, serviceInterface, connection, callback);
	}


	public ProfileFieldContainer getValueBulk(ProfileField<?>... fields) {
		if (fields == null) {
			throw new NullPointerException();
		}

		SPFError err =  new SPFError();
		try {
			ProfileFieldContainer pfc = getService().getValueBulk(getAccessToken(), ProfileField.toIdentifierList(fields), err);
			if (err.isOk()){
				return pfc;
			}
		} catch (RemoteException e) {
			onRemoteException(e);
			err.setCode(SPFError.REMOTE_EXC_ERROR_CODE);
		}
		handleError(err);
		Log.e(TAG, "Remote exception @ setValueBulk");
		return null;
	}

	public boolean setValueBulk(ProfileFieldContainer container) {
		if (container == null) {
			throw new NullPointerException();
		}
		SPFError err =  new SPFError();
		try {
			getService().setValueBulk(getAccessToken(), container, err);
			if (err.isOk()){
				return true;
			}
		} catch (RemoteException e) {
			onRemoteException(e);
			err.setCode(SPFError.REMOTE_EXC_ERROR_CODE);
		}
		handleError(err);
		return false;
	}

	private void onRemoteException(RemoteException e) {
		Log.e(TAG, "Remote exception @ setValue", e);
		getCallback().onError(new SPFError(SPFError.REMOTE_EXC_ERROR_CODE));
	}

	/**
	 * Interface of the callback to execute when the connection to the profile
	 * is ready, or an error has occurred.
	 * 
	 * @author darioarchetti
	 * 
	 */
	public interface Callback {
		public void onServiceReady(LocalProfile service);

		public void onError(SPFError errorMsg);

		public void onDisconnect();
	}

	/*
	 * Convert the custom Callback to a BaseCallback: the one utilized by the
	 * LocalComponent class.
	 * 
	 * @param callback - the custom callback
	 * 
	 * @return a BaseCallback
	 */
	private static BaseCallback<LocalProfileService> asBase(final Callback callback) {
		return new BaseCallback<LocalProfileService>() {

			@Override
			public void onServiceReady(LocalComponent<LocalProfileService> serviceInterface) {
				callback.onServiceReady((LocalProfile) serviceInterface);
			}

			@Override
			public void onError(SPFError errorMsg) {
				callback.onError(errorMsg);
			}

			@Override
			public void onDisconnect() {
				callback.onDisconnect();
			}
		};
	}
}