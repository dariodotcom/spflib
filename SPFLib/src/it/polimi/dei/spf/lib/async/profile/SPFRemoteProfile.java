/* 
 * Copyright 2014 Jacopo Aliprandi, Dario Archetti
 * 
 * This file is part of SPF.
 * 
 * SPF is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * SPF is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with SPF.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package it.polimi.dei.spf.lib.async.profile;

import android.content.Context;
import it.polimi.dei.spf.lib.async.Person;
import it.polimi.dei.spf.lib.async.SPFComponent;

/**
 * A {@link SPFComponent} that provides access to profiles of remote people to
 * allow read operation.
 * 
 * @author darioarchetti
 * 
 */
public class SPFRemoteProfile extends SPFComponent {

	private RemoteProfileInterface mInterface;

	public SPFRemoteProfile(Context context, RemoteProfileInterface iface) {
		super(context);
		this.mInterface = iface;
	}

	/**
	 * Provides a reference to a remote profile. With this reference it is
	 * possible to perform read operations and retrieve information from remote
	 * people.
	 * 
	 * @param p
	 *            - the {@link Person} whose profile to load.
	 * @return an instance of {@link RemoteProfile} to interact with the remote
	 *         profile.
	 */
	public RemoteProfile getProfileOf(Person p) {
		if (p == null) {
			throw new NullPointerException();
		}
		
		return new RemoteProfile(p, mInterface);
	}

	@Override
	protected void recycle() {
		mInterface = null;
	}
}
