/* 
 * Copyright 2014 Jacopo Aliprandi, Dario Archetti
 * 
 * This file is part of SPF.
 * 
 * SPF is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * SPF is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with SPF.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package it.polimi.dei.spf.framework.local;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

// TODO: Auto-generated Javadoc
/**
 * The Class SPFActivity.
 * 
 * @author darioarchetti
 */
public class SPFActivity implements Parcelable {

	/** The Constant VERB. */
	public static final String VERB = "verb";

	/** The Constant SENDER_IDENTIFIER. */
	public static final String SENDER_IDENTIFIER = "senderIdentifier";

	/** The Constant SENDER_DISPLAY_NAME. */
	public static final String SENDER_DISPLAY_NAME = "senderDisplayName";

	/** The Constant RECEIVER_IDENTIFIER. */
	public static final String RECEIVER_IDENTIFIER = "receiverIdentifier";

	/** The Constant RECEIVER_DISPLAY_NAME. */
	public static final String RECEIVER_DISPLAY_NAME = "receiverDisplayName";
	public static final String TIMESTAMP = "timestamp";
	public static final String SENDER_APP_IDENTIFIER = "SenderAppId";

	/** The m fields. */
	private final Map<String, String> mFields;

	/**
	 * Instantiates a new SPF activity.
	 */
	private SPFActivity() {
		this.mFields = new HashMap<String, String>();
	}

	/**
	 * Instantiates a new SPF activity.
	 * 
	 * @param verb
	 *            the verb
	 */
	public SPFActivity(String verb) {
		this();
		if (verb == null) {
			throw new NullPointerException();
		}

		mFields.put(VERB, verb);
	}

	/**
	 * Put.
	 * 
	 * @param key
	 *            the key
	 * @param value
	 *            the value
	 */
	public void put(String key, String value) {
		if (key.equals(VERB)) {
			return;
		}

		mFields.put(key, value);
	}

	/**
	 * Gets the.
	 * 
	 * @param key
	 *            the key
	 * @return the string
	 */
	public String get(String key) {
		return mFields.get(key);
	}

	/**
	 * Gets the verb.
	 * 
	 * @return the verb
	 */
	public String getVerb() {
		return mFields.get(VERB);
	}

	public Set<String> keySet() {
		return mFields.keySet();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.os.Parcelable#describeContents()
	 */
	@Override
	public int describeContents() {
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.os.Parcelable#writeToParcel(android.os.Parcel, int)
	 */
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		Bundle b = new Bundle();

		for (String k : mFields.keySet()) {
			b.putString(k, mFields.get(k));
		}

		b.writeToParcel(dest, 0);
	}

	/** The Constant CREATOR. */
	public static final Parcelable.Creator<SPFActivity> CREATOR = new Creator<SPFActivity>() {

		@Override
		public SPFActivity[] newArray(int size) {
			return new SPFActivity[size];
		}

		@Override
		public SPFActivity createFromParcel(Parcel source) {
			SPFActivity ac = new SPFActivity();
			ac.readFromParcel(source);
			return ac;
		}
	};

	/**
	 * Read from parcel.
	 * 
	 * @param parcel
	 *            the parcel
	 */
	public void readFromParcel(Parcel parcel) {
		Bundle b = parcel.readBundle();
		for (String k : b.keySet()) {
			mFields.put(k, b.getString(k));
		}
	}
}
