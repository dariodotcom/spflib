/* 
 * Copyright 2014 Jacopo Aliprandi, Dario Archetti
 * 
 * This file is part of SPF.
 * 
 * SPF is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * SPF is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with SPF.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package it.polimi.dei.spf.lib.async.services.execution;

import it.polimi.dei.spf.framework.local.InvocationRequest;
import it.polimi.dei.spf.framework.local.InvocationResponse;
import it.polimi.dei.spf.lib.async.services.ServiceInvocationException;

/**
 * Interface for components that can receive invocation requests. Used to
 * abstract the target of an invocation that may be the local instance of SPF or
 * a remote one, thus making {@link InvocationStub} able to work in both cases.
 * 
 * @author darioarchetti
 * 
 */
public interface InvocationTarget {

	/**
	 * Low level call to dispatch invocation requests.
	 * 
	 * @param request
	 *            - the {@link InvocationRequest} to be dispatched.
	 * @return an instance of {@link InvocationResponse} containing the result
	 *         of the invocation.
	 * @throws ServiceInvocationException
	 *             if an instance is thrown during the execution of the service.
	 */
	public InvocationResponse executeService(InvocationRequest request) throws ServiceInvocationException;

}
