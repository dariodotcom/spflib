/* 
 * Copyright 2014 Jacopo Aliprandi, Dario Archetti
 * 
 * This file is part of SPF.
 * 
 * SPF is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * SPF is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with SPF.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package it.polimi.dei.spf.lib.async.services.registration;

import it.polimi.dei.spf.framework.local.LocalServiceManager;
import it.polimi.dei.spf.framework.local.SPFError;
import it.polimi.dei.spf.framework.local.ServiceDescriptor;
import it.polimi.dei.spf.lib.async.AccessTokenManager;
import it.polimi.dei.spf.lib.async.LocalComponent;
import it.polimi.dei.spf.lib.async.Utils;
import it.polimi.dei.spf.lib.async.profile.LocalProfile;
import it.polimi.dei.spf.lib.async.services.ServiceInterface;
import it.polimi.dei.spf.lib.async.services.ServiceValidator;

import android.content.Context;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;

/**
 * Allows applications to register and un-registratier services, and responds to
 * execution requests coming from the SPF framework.
 * 
 * @author darioarchetti
 * 
 */
public final class LocalServiceRegistry extends LocalComponent<LocalServiceManager> {

	private static final String SERVICE_INTENT = "it.polimi.dei.spf.services.LocalServiceExecutor";
	private static final LocalComponent.Descriptor<LocalServiceManager> DESCRIPTOR = new Descriptor<LocalServiceManager>() {

		@Override
		public String getIntentName() {
			return SERVICE_INTENT;
		}

		@Override
		public LocalServiceManager castInterface(IBinder binder) {
			return LocalServiceManager.Stub.asInterface(binder);
		}

		@Override
		public LocalComponent<LocalServiceManager> createInstance(Context context, LocalServiceManager serviceInterface, ServiceConnection connection, it.polimi.dei.spf.lib.async.LocalComponent.BaseCallback<LocalServiceManager> callback) {
			return new LocalServiceRegistry(context, serviceInterface, connection, callback);
		}
	};

	/**
	 * Loads an instance of {@link LocalServiceRegistry} asynchronously. The
	 * callbacks to be called once the connection is ready should be contained
	 * into an instance of {@link LocalProfile.Callback}.
	 * 
	 * @param context
	 *            - the {@link Context} used to bind to SPF.
	 * @param callback
	 *            - the callback code to be executed once the connection is
	 *            ready.
	 */
	public static void load(final Context context, final Callback callback) {
		LocalComponent.load(context, DESCRIPTOR, asBase(callback));
	}

	protected LocalServiceRegistry(Context context, LocalServiceManager serviceInterface, ServiceConnection connection, BaseCallback<LocalServiceManager> callback) {
		super(context, serviceInterface, connection, callback);
	}

	/**
	 * Allows to register a service in the service index. Such service is made
	 * available to remote apps.
	 * 
	 * @param serviceInterface
	 * @param implementation
	 */
	public <T> void registerService(Class<? super T> serviceInterface) {
		Utils.notNull(serviceInterface, "serviceInterface must not be null");

		ServiceValidator.validate(serviceInterface, ServiceValidator.TYPE_PUBLISHED);
		ServiceInterface annotation = serviceInterface.getAnnotation(ServiceInterface.class);
		ServiceDescriptor descriptor = ServiceInterface.Convert.toServiceDescriptor(annotation);
		String token = AccessTokenManager.get(getContext()).getAccessToken();

		try {
			SPFError error = new SPFError();
			getService().registerService(token, descriptor, error);
			if (!error.isOk()) {
				handleError(error);
			}
		} catch (RemoteException e) {
			catchRemoteException(e);
		}
	}

	/**
	 * Allows to unregister a previously registered service.
	 * 
	 * @param serviceInterface
	 */
	public <T> void unregisterService(Class<? super T> serviceInterface) {
		Utils.notNull(serviceInterface, "serviceInterface must not be null");
		ServiceValidator.validate(serviceInterface, ServiceValidator.TYPE_PUBLISHED);
		ServiceInterface svcInterface = serviceInterface.getAnnotation(ServiceInterface.class);
		ServiceDescriptor svcDesc = ServiceInterface.Convert.toServiceDescriptor(svcInterface);
		String token = AccessTokenManager.get(getContext()).getAccessToken();

		try {
			SPFError error = new SPFError();
			getService().unregisterService(token, svcDesc, error);
			if(!error.isOk()){
				handleError(error);
			}
		} catch (RemoteException e) {
			catchRemoteException(e);
		}
	}

	public static interface Callback {
		public void onServiceReady(LocalServiceRegistry registry);

		public void onError(SPFError errorMsg);

		public void onDisconnect();
	}

	private static BaseCallback<LocalServiceManager> asBase(final Callback callback) {
		return new BaseCallback<LocalServiceManager>() {

			@Override
			public void onServiceReady(LocalComponent<LocalServiceManager> serviceInterface) {
				callback.onServiceReady((LocalServiceRegistry) serviceInterface);
			}

			@Override
			public void onError(SPFError errorMsg) {
				callback.onError(errorMsg);
			}

			@Override
			public void onDisconnect() {
				callback.onDisconnect();
			}
		};
	}

	private void catchRemoteException(RemoteException e) {
		disconnect();
		getCallback().onDisconnect();
	}
}