/* 
 * Copyright 2014 Jacopo Aliprandi, Dario Archetti
 * 
 * This file is part of SPF.
 * 
 * SPF is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * SPF is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with SPF.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package it.polimi.dei.spf.framework.local;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

// TODO: Auto-generated Javadoc
/**
 * The Class SPFActionSendNotification.
 *
 * @author Jacopo Aliprandi
 */
public final class SPFActionSendNotification extends SPFAction {

	/** The Constant KEY_MESSAGE. */
	private static final String KEY_MESSAGE = "message";

	/** The Constant KEY_TITLE. */
	private static final String KEY_TITLE = "title";

	/** The Constant TAG. */
	private static final String TAG = null;

	/** The m type. */
	private final int mType = TYPE_NOTIFICATION;

	/**
	 * The title to show in the notification.
	 */
	private final String mTitle;

	/**
	 * the message to show in the notification.
	 */
	private final String mMessage;

	/*
	 * other properties can be: icon sound intent when the notification is
	 * pressed ...
	 */

	/**
	 * TODO comment.
	 *
	 * @param title the title
	 * @param message the message
	 */
	public SPFActionSendNotification(String title, String message) {
		this.mTitle = title;
		this.mMessage = message;
	}

	/**
	 * Instantiates a new SPF action send notification.
	 *
	 * @param source the source
	 */
	private SPFActionSendNotification(Parcel source) {
		this.mTitle = source.readString();
		this.mMessage = source.readString();
	}

	/**
	 * Instantiates a new SPF action send notification.
	 *
	 * @param source the source
	 */
	public SPFActionSendNotification(JSONObject source) {
		try {
			mTitle = source.getString(KEY_TITLE);
			mMessage = source.getString(KEY_MESSAGE);
		} catch (JSONException e) {
			throw new IllegalArgumentException("JSON does not represent a valid SPFActionNotification");
		}
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return mTitle;
	}

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage() {
		return mMessage;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.os.Parcelable#describeContents()
	 */
	@Override
	public int describeContents() {
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.os.Parcelable#writeToParcel(android.os.Parcel, int)
	 */
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(mTitle);
		dest.writeString(mMessage);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.polimi.dei.spf.framework.local.SPFAction#toJSON()
	 */
	@Override
	public String toJSON() {
		try {
			JSONObject o = new JSONObject();
			o.put(KEY_TYPE, mType);
			o.put(KEY_TITLE, mTitle);
			o.put(KEY_MESSAGE, mMessage);
			return o.toString();
		} catch (JSONException e) {
			Log.e(TAG, "Error marshalling:", e);
			return "";
		}
	}

	/** The creator. */
	public static Parcelable.Creator<SPFActionSendNotification> CREATOR = new Creator<SPFActionSendNotification>() {

		@Override
		public SPFActionSendNotification[] newArray(int size) {
			return new SPFActionSendNotification[size];
		}

		@Override
		public SPFActionSendNotification createFromParcel(Parcel source) {
			return new SPFActionSendNotification(source);
		}
	};

}
