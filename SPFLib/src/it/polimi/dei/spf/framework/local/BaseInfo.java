/* 
 * Copyright 2014 Jacopo Aliprandi, Dario Archetti
 * 
 * This file is part of SPF.
 * 
 * SPF is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * SPF is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with SPF.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package it.polimi.dei.spf.framework.local;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

// TODO: Auto-generated Javadoc
/**
 * Container for basic information about a person that is transmitted together
 * with search results.
 * 
 * BaseInfo carries the following details:
 * <ul>
 * <li>Identifier</li>
 * <li>Display name</li>
 * </ul>
 * 
 * @author darioarchetti
 * 
 */
public class BaseInfo implements Parcelable{

	/** The Constant DISPLAY_NAME. */
	private static final String DISPLAY_NAME = "displayName";
	
	/** The Constant IDENTIFIER. */
	private static final String IDENTIFIER = "identifier";
	
	/** The Constant TAG. */
	private static final String TAG = "BaseInfo";
	
	/** The m display name. */
	private final String mIdentifier, mDisplayName;

	/**
	 * Unflattens a {@link BaseInfo} instance from a JSON representation created
	 * with {@link BaseInfo#toJSONString()}.
	 *
	 * @param json            - the JSON string representing the original instance
	 * @return the unflettened instance
	 */
	public static BaseInfo fromJSONString(String json) {
		try {
			JSONObject o = new JSONObject(json);
			return new BaseInfo(o.getString(IDENTIFIER), o.getString(DISPLAY_NAME));
		} catch (JSONException e) {
			Log.d(TAG, "Error unmarshalling baseinfo: ", e);
			return null;
		}
	}

	/**
	 * Creates a new instance of {@link BaseInfo} with the provided details.
	 * 
	 * @param mIdentifier
	 *            - the identifier (see {@link ProfileField#IDENTIFIER})
	 * @param mDisplayName
	 *            - the display name (see {@link ProfileField#DISPLAY_NAME})
	 */
	public BaseInfo(String mIdentifier, String mDisplayName) {
		this.mIdentifier = mIdentifier;
		this.mDisplayName = mDisplayName;
	}

	/**
	 * Instantiates a new base info.
	 *
	 * @param source the source
	 */
	public BaseInfo(Parcel source) {
		mIdentifier = source.readString();
		mDisplayName = source.readString();
	}

	/**
	 * Gets the identifier.
	 *
	 * @return the identifier
	 */
	public String getIdentifier() {
		return mIdentifier;
	}

	/**
	 * Gets the display name.
	 *
	 * @return the display name
	 */
	public String getDisplayName() {
		return mDisplayName;
	}

	/**
	 * Flattens the instance into a JSON String that can be later unflattened
	 * with {@link BaseInfo#fromJSONString(String)}.
	 *
	 * @return - the flattened representation
	 */
	public String toJSONString() {
		JSONObject object = new JSONObject();

		try {
			object.put(IDENTIFIER, mIdentifier);
			object.put(DISPLAY_NAME, mDisplayName);
		} catch (JSONException e) {
			Log.d(TAG, "Error marshalling baseinfo", e);
		}

		return object.toString();
	}

	
	/* (non-Javadoc)
	 * @see android.os.Parcelable#describeContents()
	 */
	@Override
	public int describeContents() {
		return 0;
	}

	/* (non-Javadoc)
	 * @see android.os.Parcelable#writeToParcel(android.os.Parcel, int)
	 */
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(mIdentifier);
		dest.writeString(mDisplayName);
	}
	
	/** The Constant CREATOR. */
	public static final Parcelable.Creator<BaseInfo> CREATOR = new Creator<BaseInfo>() {
		
		@Override
		public BaseInfo[] newArray(int size) {
			return new BaseInfo[size];
		}
		
		@Override
		public BaseInfo createFromParcel(Parcel source) {
			return new BaseInfo(source);
		}
	};
}
