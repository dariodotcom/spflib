/* 
 * Copyright 2014 Jacopo Aliprandi, Dario Archetti
 * 
 * This file is part of SPF.
 * 
 * SPF is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * SPF is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with SPF.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package it.polimi.dei.spf.framework.local;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.graphics.Bitmap;

// TODO: Auto-generated Javadoc
/**
 * Instances of this class represents the type of fields supported by the local
 * profile, and are used to specify which field to write/read in components to
 * access profile. Instances are available as static methods of this class. The
 * list of available fields, with their type, are
 * <ul>
 * <li>IDENTIFIER:string</li>
 * <li>DISPLAY_NAME:string</li>
 * <li>PHOTO:bitmap</li>
 * <li>BIRTHDAY:Date</li>
 * <li>ABOUT_ME:string</li>
 * <li>EMAILS:string[]</li>
 * <li>LOCATION:string</li>
 * <li>STATUS:string</li>
 * <li>GENDER:string</li>
 * <li>INTERESTS:string[]</li>
 * </ul>
 *
 * @param <E> the element type
 */
public class ProfileField<E> {

	/** The s fields. */
	private static List<ProfileField<?>> sFields = new ArrayList<ProfileField<?>>();

	// Available fields
	/** The identifier. */
	public static ProfileField<String> IDENTIFIER = new ProfileField<String>("identifier", String.class);
	
	/** The display name. */
	public static ProfileField<String> DISPLAY_NAME = new ProfileField<String>("display_name", String.class);
	
	/** The photo. */
	public static ProfileField<Bitmap> PHOTO = new ProfileField<Bitmap>("photo", Bitmap.class);
	
	/** The birthday. */
	public static ProfileField<Date> BIRTHDAY = new DateProfileField("birthday");
	
	/** The about me. */
	public static ProfileField<String> ABOUT_ME = new ProfileField<String>("about_me", String.class);
	
	/** The emails. */
	public static ProfileField<String[]> EMAILS = new ProfileField<String[]>("emails", String[].class);
	
	/** The location. */
	public static ProfileField<String> LOCATION = new ProfileField<String>("location", String.class);
	
	/** The status. */
	public static ProfileField<String> STATUS = new ProfileField<String>("status", String.class);
	
	/** The gender. */
	public static ProfileField<String> GENDER = new MultipleChoicheProfileField<String>("gender", String.class, new String[] { "male", "female", "other" });
	
	/** The interests. */
	public static ProfileField<String[]> INTERESTS = new TagProfileField("interests");

	/**
	 * Gets the default fields.
	 *
	 * @return the default fields
	 */
	public static List<ProfileField<?>> getDefaultFields() {
		return new ArrayList<ProfileField<?>>(sFields);
	}

	/**
	 * To identifier list.
	 *
	 * @param fields the fields
	 * @return the string[]
	 */
	public static String[] toIdentifierList(ProfileField<?>[] fields) {
		if (fields == null) {
			return null;
		}

		String[] identifiers = new String[fields.length];
		for (int i = 0; i < fields.length; i++) {
			identifiers[i] = fields[i].getIdentifier();
		}

		return identifiers;
	}

	/**
	 * From identifier list.
	 *
	 * @param identifiers the identifiers
	 * @return the profile field[]
	 */
	public static ProfileField<?>[] fromIdentifierList(String[] identifiers) {
		if (identifiers == null) {
			return null;
		}

		ProfileField<?>[] fields = new ProfileField<?>[identifiers.length];
		for (int i = 0; i < identifiers.length; i++) {
			fields[i] = lookup(identifiers[i]);

			if (fields[i] == null) {
				throw new IllegalArgumentException("Unknown field " + identifiers[i]);
			}
		}

		return fields;
	}

	/**
	 * Lookup.
	 *
	 * @param identifier the identifier
	 * @return the profile field
	 */
	public static ProfileField<?> lookup(String identifier) {
		for (ProfileField<?> field : sFields) {
			if (field.getIdentifier().equals(identifier)) {
				return field;
			}
		}

		return null;
	}

	/** The m identifier. */
	private String mIdentifier;
	
	/** The m class. */
	private Class<E> mClass;

	/**
	 * Instantiates a new profile field.
	 *
	 * @param identifier the identifier
	 * @param fieldClass the field class
	 */
	private ProfileField(String identifier, Class<E> fieldClass) {
		this.mIdentifier = identifier;
		this.mClass = fieldClass;
		sFields.add(this);
	}

	/**
	 * Gets the identifier.
	 *
	 * @return the identifier
	 */
	public String getIdentifier() {
		return mIdentifier;
	}

	/**
	 * Gets the field class.
	 *
	 * @return the field class
	 */
	public Class<E> getFieldClass() {
		return mClass;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "[ProfileField: " + mIdentifier + "]";
	}
	
	/**
	 * The Class MultipleChoicheProfileField.
	 *
	 * @param <E> the element type
	 */
	public static class MultipleChoicheProfileField<E> extends ProfileField<E> {

		/** The m choiches. */
		private final E[] mChoiches;

		/**
		 * Instantiates a new multiple choiche profile field.
		 *
		 * @param identifier the identifier
		 * @param fieldClass the field class
		 * @param choiches the choiches
		 */
		public MultipleChoicheProfileField(String identifier, Class<E> fieldClass, E[] choiches) {
			super(identifier, fieldClass);
			this.mChoiches = choiches;
		}

		/**
		 * Gets the choiches.
		 *
		 * @return the choiches
		 */
		public E[] getChoiches() {
			return mChoiches;
		}
	}

	/**
	 * The Class TagProfileField.
	 */
	public static class TagProfileField extends ProfileField<String[]> {
		
		/**
		 * Instantiates a new tag profile field.
		 *
		 * @param identifier the identifier
		 */
		public TagProfileField(String identifier) {
			super(identifier, String[].class);
		}
	}
	
	/**
	 * The Class DateProfileField.
	 */
	public static class DateProfileField extends ProfileField<Date> {
		
		/**
		 * Instantiates a new date profile field.
		 *
		 * @param identifier the identifier
		 */
		public DateProfileField(String identifier){
			super(identifier,Date.class);
		}
	}
}