/* 
 * Copyright 2014 Jacopo Aliprandi, Dario Archetti
 * 
 * This file is part of SPF.
 * 
 * SPF is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * SPF is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with SPF.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package it.polimi.dei.spf.lib.async;

import it.polimi.dei.spf.framework.local.InvocationRequest;
import it.polimi.dei.spf.framework.local.InvocationResponse;
import it.polimi.dei.spf.framework.local.ProfileFieldContainer;
import it.polimi.dei.spf.framework.local.SPFActivity;
import it.polimi.dei.spf.framework.local.SPFError;
import it.polimi.dei.spf.framework.local.SPFProximityService;
import it.polimi.dei.spf.framework.local.SPFSearchCallback;
import it.polimi.dei.spf.framework.local.SearchDescriptor;
import it.polimi.dei.spf.lib.async.profile.RemoteProfileInterface;
import it.polimi.dei.spf.lib.async.profile.SPFRemoteProfile;
import it.polimi.dei.spf.lib.async.search.SPFSearch;
import it.polimi.dei.spf.lib.async.search.SearchInterface;
import it.polimi.dei.spf.lib.async.services.execution.SPFServiceExecutor;
import it.polimi.dei.spf.lib.async.services.execution.ServiceExecutionInterface;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

/**
 * Class that provides a unified access to components for the interaction of
 * people in proximity. The component is loaded in a static way using the
 * {@link #connect(Context, ConnectionListener)} method.
 * 
 * @author darioarchetti
 * 
 */
public class SPF implements SearchInterface, RemoteProfileInterface, ServiceExecutionInterface {

	public static final int SEARCH_COMPONENT = 0;
	public static final int SERVICE_EXECUTION_COMPONENT = 1;
	public static final int REMOTE_PROFILE_COMPONENT = 2;
	private final static String SERVER_INTENT = "it.polimi.dei.spf.framework.local.SPFServerService";
	private static final String TAG = "SPF";

	/**
	 * Creates a connection to SPF asynchronously.
	 * 
	 * @param context
	 *            - the context used to bind to SPF service.
	 * @param listener
	 *            - the listener that is notified when the connection to SPF is
	 *            ready, when the connection is closed or when an error occurs.
	 */
	public static void connect(final Context context, final ConnectionListener listener) {
		Utils.notNull(context, "Context must not be null");
		Utils.notNull(listener, "Listener must not be null");

		if (AccessTokenManager.get(context).hasToken()) {
			doConnect(context, listener);
		} else {
			AccessTokenManager.get(context).requireAccessToken(context, new AccessTokenManager.RegistrationCallback() {

				@Override
				public void onRegistrationSuccessful() {
					doConnect(context, listener);
				}

				@Override
				public void onRegistrationError(SPFError errorMsg) {
					listener.onError(errorMsg);
				}
			});
		}

	}

	private static void doConnect(final Context context, final ConnectionListener listener) {
		ServiceConnection conn = new ServiceConnection() {
			@Override
			public void onServiceDisconnected(ComponentName name) {
				listener.onDisconnected();
				// TODO lifecycle management
			}

			@Override
			public void onServiceConnected(ComponentName name, IBinder service) {
				SPFProximityService spfServer = SPFProximityService.Stub.asInterface(service);
				SPF instance = new SPF(context, spfServer, this);
				try {
					instance.init();
					listener.onConnected(instance);
				} catch (RemoteException e) {
					listener.onError(new SPFError(SPFError.REMOTE_EXC_ERROR_CODE));
				}
			}
		};

		Intent intent = new Intent(SERVER_INTENT);
		if (context.bindService(intent, conn, Context.BIND_AUTO_CREATE) == false) {
			listener.onError(new SPFError(SPFError.SPF_NOT_INSTALLED_ERROR_CODE));
			Log.d(TAG, "Binding to SPFService failed");
		}

	}

	private SPFProximityService mService;
	private SPFSearchCallback callback;
	private Context mContext;
	private ServiceConnection mConnection;

	// Components
	private SPFSearch spfSearch;
	private SPFRemoteProfile spfRemoteProfile;
	private SPFServiceExecutor spfServiceExecutor;

	private SPF(Context context, SPFProximityService serverService, ServiceConnection serviceConnection) {
		this.mContext = context;
		this.mService = serverService;
		this.mConnection = serviceConnection;
	}

	protected void init() throws RemoteException {
		spfSearch = new SPFSearch(mContext, this);
		callback = new SPFSearchCallbackImpl(spfSearch);

		SPFError err = new SPFError();
		mService.registerCallback(getAccessToken(), callback, err);
		if (!err.isOk()) {
			handleError(err);
		}
	}

	/**
	 * Provides access to the different components of SPF. The components are
	 * identified by static constants available in this class, and all share a
	 * common superclass, {@link SPFComponent}; the instances returned vy this
	 * method should be casted to the right class. Available services are:
	 * <ul>
	 * <li>{@link #SEARCH_COMPONENT}: Searching of people in proximity. Returned
	 * instance should be casted to {@link SPFSearch}</li>
	 * <li>{@link #SERVICE_EXECUTION_COMPONENT}: Execution of remote methods.
	 * Returned instance should be casted to {@link SPFServiceExecutor}</li>
	 * <li>{@link #REMOTE_PROFILE_COMPONENT}: Retrieval of information from
	 * remote profiles. Returned instances should be casted to
	 * {@link SPFRemoteProfile}</li>
	 * </ul>
	 * 
	 * @param code
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public synchronized <E extends SPFComponent> E getComponent(int code) {
		switch (code) {
		case REMOTE_PROFILE_COMPONENT:
			if (spfRemoteProfile == null) {
				spfRemoteProfile = new SPFRemoteProfile(getContext(), this);
			}
			return (E) spfRemoteProfile;

		case SEARCH_COMPONENT:
			if (spfSearch == null) {
				spfSearch = new SPFSearch(getContext(), this);
			}
			return (E) spfSearch;
		case SERVICE_EXECUTION_COMPONENT:
			if (spfServiceExecutor == null) {
				spfServiceExecutor = new SPFServiceExecutor(getContext(), this);
			}
			return (E) spfServiceExecutor;
		default:
			throw new IllegalArgumentException("Component code " + code + " not found.");
		}
	}

	/**
	 * Disconnects from SPF. All created components will stop working after this
	 * method is invoked.
	 */
	public void disconnect() {
		try {
			SPFError err = new SPFError();
			mService.unregisterCallback(getAccessToken(), err);
			if (err.isOk()) {
				handleError(err);
			}

			// TODO release resources
			// spfSearch.recycle();
			// spfRemoteProfile.recycle();
			// spfServiceExecutor.recycle();

		} catch (RemoteException e) {
			// no code here
		}
		mContext.unbindService(mConnection);
	}

	/**
	 * @return the context used to connect to SPF.
	 */
	public Context getContext() {
		return mContext;
	}

	/*
	 * Implementation of SearchInterface (non-Javadoc)
	 * 
	 * @see
	 * it.polimi.dei.spf.lib.async.search.SearchInterface#startSearch(it.polimi
	 * .dei.spf.framework.local.SearchDescriptor,
	 * it.polimi.dei.spf.lib.async.search.SearchInterface.SearchStartedCallback)
	 */
	@Override
	public void startSearch(SearchDescriptor searchDescriptor, SearchStartedCallback callback) {
		try {
			SPFError err = new SPFError();
			String queryId = mService.startNewSearch(getAccessToken(), searchDescriptor, err);
			if (err.isOk()) {
				callback.run(queryId);
			} else {
				handleError(err);
			}
		} catch (RemoteException e) {
			// TODO Error management
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.polimi.dei.spf.lib.async.search.SearchInterface#stopSearch(java.lang
	 * .String)
	 */
	@Override
	public void stopSearch(String queryId) {
		try {
			SPFError err = new SPFError();
			mService.stopSearch(getAccessToken(), queryId, err);
			if (!err.isOk()) {
				handleError(err);
			}
		} catch (RemoteException e) {
			// TODO: error management
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.polimi.dei.spf.lib.async.search.SearchInterface#lookup(java.lang.String
	 * )
	 */
	@Override
	public boolean lookup(String identifier) {
		try {
			SPFError err = new SPFError();
			boolean found = mService.lookup(getAccessToken(), identifier, err);
			if (err.isOk()) {
				return found;
			} else {
				handleError(err);
				return false;
			}
		} catch (RemoteException e) {
			// TODO Error Management
			Log.d(TAG, "Remote exception while executing lookup on " + identifier, e);
			return false;
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.polimi.dei.spf.lib.async.profile.RemoteProfileInterface#getProfileBulk
	 * (java.lang.String, java.lang.String[],
	 * it.polimi.dei.spf.framework.local.SPFError)
	 */
	@Override
	public ProfileFieldContainer getProfileBulk(String identifier, String[] fields, SPFError err) {
		String accessToken = AccessTokenManager.get(mContext).getAccessToken();
		try {
			ProfileFieldContainer pfc = mService.getProfileBulk(accessToken, identifier, fields, err);
			if (err.isOk()) {
				return pfc;
			}
		} catch (RemoteException e) {
			Log.e(TAG, "Error @ getProfileBulk", e);
		}
		handleError(err);
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.polimi.dei.spf.lib.async.services.execution.ServiceExecutionInterface
	 * #executeService(java.lang.String,
	 * it.polimi.dei.spf.framework.local.InvocationRequest,
	 * it.polimi.dei.spf.framework.local.SPFError)
	 */
	@Override
	public InvocationResponse executeService(String target, InvocationRequest request) {
		try {
			SPFError err = new SPFError();
			String token = AccessTokenManager.get(mContext).getAccessToken();

			for (Object param : request.getParams()) {
				if (param instanceof SPFActivity) {
					mService.injectInformationIntoActivity(token, target, (SPFActivity) param, err);
					if (!err.isOk()) {
						handleError(err);
						return InvocationResponse.error(err.getMessage());
					}
				}
			}

			InvocationResponse resp = mService.executeRemoteService(token, target, request, err);
			if (!err.isOk()) {
				handleError(err);
				return InvocationResponse.error(err.getMessage());
			}
			return resp;
		} catch (RemoteException e) {
			return InvocationResponse.error(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.polimi.dei.spf.lib.async.services.execution.ServiceExecutionInterface
	 * #sendActivity(it.polimi.dei.spf.framework.local.SPFActivity)
	 */
	@Override
	public boolean sendActivity(String target, SPFActivity activity) {
		String token = getAccessToken();
		SPFError err = new SPFError();
		InvocationResponse resp;

		try {
			mService.injectInformationIntoActivity(token, target, activity, err);
			if (!err.isOk()) {
				handleError(err);
				return false;
			}

			resp = mService.sendActivity(token, target, activity, err);
		} catch (RemoteException e) {
			// TODO Error Management
			return false;
		}

		if (!err.isOk()) {
			handleError(err);
			return false;
		}

		if (!resp.isResult()) {
			return false;
		}

		return (Boolean) resp.getResult();
	}

	private void handleError(SPFError err) {
		if (err.codeEquals(SPFError.TOKEN_NOT_VALID_ERROR_CODE)) {
			AccessTokenManager.get(mContext).invalidateToken();
		}

	}

	private String getAccessToken() {
		return AccessTokenManager.get(mContext).getAccessToken();
	}

	/**
	 * Interface for components that wants to be notified of the status of the
	 * connection with SPF.
	 * 
	 * @author darioarchetti
	 * 
	 */
	public static interface ConnectionListener {

		/**
		 * Called when the connection to SPF is available.
		 * 
		 * @param instance
		 *            - the instance of SPF to use to interact with people in
		 *            the proximity.
		 */
		public void onConnected(SPF instance);

		/**
		 * Called when an error occurs.
		 */
		public void onError(SPFError errorMsg);

		/**
		 * Called when the connection with SPF is closed.
		 */
		public void onDisconnected();

	}
}
