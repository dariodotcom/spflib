/* 
 * Copyright 2014 Jacopo Aliprandi, Dario Archetti
 * 
 * This file is part of SPF.
 * 
 * SPF is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * SPF is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with SPF.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package it.polimi.dei.spf.lib.async.search;

import java.util.Hashtable;
import java.util.Map;

import android.content.Context;
import android.util.SparseArray;
import it.polimi.dei.spf.framework.local.BaseInfo;
import it.polimi.dei.spf.framework.local.SearchDescriptor;
import it.polimi.dei.spf.lib.async.LooperUtils;
import it.polimi.dei.spf.lib.async.Person;
import it.polimi.dei.spf.lib.async.SPFComponent;

/**
 * A {@link SPFComponent} that allows applications to search for remote people
 * who match given criteria.
 * 
 */
public final class SPFSearch extends SPFComponent {

	private SparseArray<String> mTagToId;
	private SearchInterface mSearchInterface;
	private Map<String, SearchCallback> mCallbacks;

	// TODO @hide
	public SPFSearch(Context context, SearchInterface searchInterface) {
		super(context);
		mTagToId = new SparseArray<String>();
		mCallbacks = new Hashtable<String, SPFSearch.SearchCallback>();
		mSearchInterface = searchInterface;
	}

	/**
	 * Starts a search for remote people. The application must provide a tag,
	 * used to replace equivalent queries, a {@link SearchDescriptor} containing
	 * the configuration of the search, and a {@link SearchCallback} to be
	 * notified of found results.
	 * 
	 * @param tag
	 *            - the tag identifying this query
	 * @param searchDescriptor
	 *            - the descriptor of the search
	 * @param callback
	 *            - the callback to be notified of results.
	 */
	public void startSearch(final int tag, SearchDescriptor searchDescriptor, final SearchCallback callback) {
		// replace equivalent query ( equivalent == same tag )
		if (mTagToId.get(tag) != null) {
			String queryId = mTagToId.get(tag);
			mTagToId.delete(tag);
			if (queryId != null && mCallbacks.remove(queryId) != null) {
				mSearchInterface.stopSearch(queryId);
			}
		}
		mSearchInterface.startSearch(searchDescriptor, new SearchInterface.SearchStartedCallback() {

			@Override
			public void run(String queryId) {
				if (queryId == null) {
					callback.onSearchError();
					return;
				}

				SearchCallback uiCallback = LooperUtils.onMainThread(SearchCallback.class, callback);
				
				mCallbacks.put(queryId, uiCallback);
				mTagToId.put(tag, queryId);
				uiCallback.onSearchStart();
			}
		});

	}

	/**
	 * Stops a previously registered search request performed by the
	 * application. The application must provide the tag it registered the
	 * search with. The callback associated to the search does not receive any
	 * further notification.
	 * 
	 * @param tag
	 *            - the tag used to register the search.
	 */
	public void stopSearch(int tag) {
		String queryId = mTagToId.get(tag);
		mTagToId.delete(tag);
		if (queryId != null && mCallbacks.remove(queryId) != null) {
			mSearchInterface.stopSearch(queryId);
		}
	}

	/**
	 * Stops all searches registered by the application.
	 * 
	 * @see SPFSearch#stopSearch(int)
	 */
	public void stopAllSearch() {
		mTagToId.clear();
		String[] queryIds = (String[]) mCallbacks.keySet().toArray();
		mCallbacks.clear();
		for (String queryId : queryIds) {
			mSearchInterface.stopSearch(queryId);
		}
	}

	/**
	 * Allows to retrieve a reference to a remote person given its identifier.
	 * This reference is valid until the given person is reachable from the
	 * proximity middleware.
	 * 
	 * @param identifier
	 * @return
	 */
	public Person lookupFoundPerson(String identifier) {

		boolean isReachable = mSearchInterface.lookup(identifier);
		if (isReachable) {
			return new Person(identifier);
		} else {
			return null;
		}

	}

	/**
	 * Interface for components that can receive updates on a registered search
	 * request.
	 * 
	 * @author darioarchetti
	 * 
	 */
	public interface SearchCallback {
		/**
		 * Called when a person matching the criteria of the search is found.
		 * 
		 * @param p
		 *            - the found person.
		 */
		void onPersonFound(Person p);

		/**
		 * Called when the contact with a previously found person is lost.
		 * 
		 * @param p
		 *            - the person we are no more in contact with.
		 */
		void onPersonLost(Person p);

		/**
		 * Called when the search is stopped.
		 */
		void onSearchStop();

		/**
		 * Called when an error occurs during the search.
		 */
		void onSearchError();

		void onSearchStart();
	}

	// TODO hide
	public void onResultLost(String queryId, String uniqueIdentifier) {
		SearchCallback callback = mCallbacks.get(queryId);
		if (callback == null) {
			return;
		}
		callback.onPersonLost(new Person(uniqueIdentifier));
	}

	// TODO hide
	public void onResultFound(String queryId, String uniqueIdentifier, BaseInfo baseInfo) {
		SearchCallback callback = mCallbacks.get(queryId);
		if (callback == null) {
			mSearchInterface.stopSearch(queryId);
			return;
		}
		Person p = new Person(uniqueIdentifier, baseInfo);
		callback.onPersonFound(p);

	}

	// TODO hide
	public void onStop(String queryId) {
		SearchCallback callback = mCallbacks.get(queryId);
		if (callback == null) {
			mSearchInterface.stopSearch(queryId);
			return;
		}
		
		callback.onSearchStop();
	}

	// TODO hide
	public void onError(String queryId) {
		SearchCallback callback = mCallbacks.get(queryId);
		if (callback == null) {
			mSearchInterface.stopSearch(queryId);
			return;
		}
		
		callback.onSearchError();
	}

	public void onSearchStart(String queryId) {
		SearchCallback callback = mCallbacks.get(queryId);
		if (callback == null) {
			mSearchInterface.stopSearch(queryId);
			return;
		}
		
		callback.onSearchStart();
	}

	@Override
	protected void recycle() {
		// TODO Auto-generated method stub

	}

}
