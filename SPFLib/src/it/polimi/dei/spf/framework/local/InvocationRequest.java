/* 
 * Copyright 2014 Jacopo Aliprandi, Dario Archetti
 * 
 * This file is part of SPF.
 * 
 * SPF is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * SPF is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with SPF.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package it.polimi.dei.spf.framework.local;

import java.util.Arrays;

import android.os.Parcel;
import android.os.Parcelable;

// TODO: Auto-generated Javadoc
/**
 * The Class InvocationRequest.
 */
public class InvocationRequest implements Parcelable {

	/** The app name. */
	private String appName;
	
	/** The service name. */
	private String serviceName;
	
	/** The method name. */
	private String methodName;
	
	/** The params. */
	private Object[] params;

	/**
	 * Instantiates a new invocation request.
	 *
	 * @param appName the app name
	 * @param serviceName the service name
	 * @param methodName the method name
	 * @param params the params
	 */
	public InvocationRequest(String appName, String serviceName, String methodName, Object[] params) {
		this.appName = appName;
		this.serviceName = serviceName;
		this.methodName = methodName;
		this.params = params;
	}

	/**
	 * Instantiates a new invocation request.
	 *
	 * @param source the source
	 */
	private InvocationRequest(Parcel source) {
		appName = source.readString();
		serviceName = source.readString();
		methodName = source.readString();
		params = source.readArray(null);
	}

	/**
	 * Gets the app name.
	 *
	 * @return the app name
	 */
	public String getAppName() {
		return appName;
	}

	/**
	 * Gets the service name.
	 *
	 * @return the service name
	 */
	public String getServiceName() {
		return serviceName;
	}

	/**
	 * Gets the method name.
	 *
	 * @return the method name
	 */
	public String getMethodName() {
		return methodName;
	}

	/**
	 * Gets the params.
	 *
	 * @return the params
	 */
	public Object[] getParams() {
		return params;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return appName + " - " + serviceName + "." + methodName + Arrays.toString(params);
	}

	/* (non-Javadoc)
	 * @see android.os.Parcelable#describeContents()
	 */
	@Override
	public int describeContents() {
		return 0;
	}

	/* (non-Javadoc)
	 * @see android.os.Parcelable#writeToParcel(android.os.Parcel, int)
	 */
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(appName);
		dest.writeString(serviceName);
		dest.writeString(methodName);
		dest.writeArray(params);
	}

	/** The creator. */
	public static Parcelable.Creator<InvocationRequest> CREATOR = new Creator<InvocationRequest>() {

		@Override
		public InvocationRequest[] newArray(int size) {
			return new InvocationRequest[size];
		}

		@Override
		public InvocationRequest createFromParcel(Parcel source) {
			return new InvocationRequest(source);
		}
	};
	
}
