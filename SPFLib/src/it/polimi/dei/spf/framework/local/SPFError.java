/* 
 * Copyright 2014 Jacopo Aliprandi, Dario Archetti
 * 
 * This file is part of SPF.
 * 
 * SPF is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * SPF is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with SPF.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package it.polimi.dei.spf.framework.local;

import android.os.Parcel;
import android.os.Parcelable;

// TODO: Auto-generated Javadoc
/**
 * This class defines the standards errors signaled by the framework.
 * 
 */
public class SPFError implements Parcelable {

	/** The Constant NONE_ERROR_CODE. */
	public static final int NONE_ERROR_CODE = 0;
	
	/** The Constant TOKEN_NOT_VALID_ERROR_CODE. */
	public static final int TOKEN_NOT_VALID_ERROR_CODE = 1;
	
	/** The Constant PERMISSION_DENIED_ERROR_CODE. */
	public static final int PERMISSION_DENIED_ERROR_CODE = 2;
	
	/** The Constant REMOTE_EXC_ERROR_CODE. */
	public static final int REMOTE_EXC_ERROR_CODE = 3;
	
	/** The Constant INSTANCE_NOT_FOUND_ERROR_CODE. */
	public static final int INSTANCE_NOT_FOUND_ERROR_CODE = 4;
	
	/** The Constant NETWORK_ERROR_CODE. */
	public static final int NETWORK_ERROR_CODE = 5;
	
	/** The Constant ILLEGAL_ARGUMENT_ERROR_CODE. */
	public static final int ILLEGAL_ARGUMENT_ERROR_CODE = 6;
	
	/** The Constant REGISTRATION_REFUSED_ERROR_CODE. */
	public static final int REGISTRATION_REFUSED_ERROR_CODE = 7;
	
	/** The Constant INTERNAL_SPF_ERROR_CODE. */
	public static final int INTERNAL_SPF_ERROR_CODE = 8;
	
	/** The Constant SPF_NOT_INSTALLED_ERROR_CODE. */
	public static final int SPF_NOT_INSTALLED_ERROR_CODE = 9;
	
	/** The Constant APP_IDENTIFIER_MISMATCH. */
	public static final int APP_IDENTIFIER_MISMATCH = 10;

	/** The code. */
	private int code;
	
	/** The message. */
	private String message;

	/**
	 * Instantiates a new SPF error.
	 *
	 * @param errorCode the error code
	 */
	public SPFError(int errorCode) {
		this.code = errorCode;
	}

	/**
	 * Instantiates a new SPF error.
	 */
	public SPFError() {
		this.code = NONE_ERROR_CODE;
	};

	/**
	 * Code equals.
	 *
	 * @param errorCode the error code
	 * @return true, if successful
	 */
	public boolean codeEquals(int errorCode) {
		return this.code == errorCode;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + code;
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof SPFError)) {
			return false;
		}
		SPFError other = (SPFError) obj;
		if (code != other.code) {
			return false;
		}
		return true;
	};

	/**
	 * Instantiates a new SPF error.
	 *
	 * @param source the source
	 */
	SPFError(Parcel source) {
		this.code = source.readInt();
	}

	/** The creator. */
	public static Parcelable.Creator<SPFError> CREATOR = new Creator<SPFError>() {

		@Override
		public SPFError[] newArray(int size) {

			return new SPFError[size];
		}

		@Override
		public SPFError createFromParcel(Parcel source) {
			return new SPFError(source);
		}
	};

	/* (non-Javadoc)
	 * @see android.os.Parcelable#describeContents()
	 */
	@Override
	public int describeContents() {
		return 0;
	}

	/* (non-Javadoc)
	 * @see android.os.Parcelable#writeToParcel(android.os.Parcel, int)
	 */
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(code);
		dest.writeString(message);
	}

	/**
	 * Read from parcel.
	 *
	 * @param source the source
	 */
	void readFromParcel(Parcel source) {
		this.code = source.readInt();
		this.message = source.readString();
	}

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public int getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code            the code to set
	 */
	public void setCode(int code) {
		this.code = code;
	}

	/**
	 * Sets the error.
	 *
	 * @param code the code
	 * @param message the message
	 */
	public void setError(int code, String message) {
		this.code = code;
		this.message = message;
	}

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Checks if is ok.
	 *
	 * @return true, if is ok
	 */
	public boolean isOk() {
		return code == NONE_ERROR_CODE;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String string ;
		switch(code){
			case NONE_ERROR_CODE: 
				string = "NONE_ERROR";
				break;
			case TOKEN_NOT_VALID_ERROR_CODE:
				string = "TOKEN_NOT_VALID";
				break;
			case PERMISSION_DENIED_ERROR_CODE:
				string = "PERMISSION_DENIED";
				break;
			case REMOTE_EXC_ERROR_CODE:
				string = "REMOTE_EXCEPTION";
				break;
			case INSTANCE_NOT_FOUND_ERROR_CODE:
				string = "INSTANCE_NOT_FOUND";
				break;
			case NETWORK_ERROR_CODE:
				string = "NETWORK_ERROR";
				break;
			case ILLEGAL_ARGUMENT_ERROR_CODE:
				string = "ILLEGAL_ARUMENT";
				break;
			case REGISTRATION_REFUSED_ERROR_CODE:
				string = "REGISTRATION_REFUSED";
				break;
			case INTERNAL_SPF_ERROR_CODE:
				string = "INTERNAL_SPF_ERROR";
				break;
			default:
				string = "UNKNOWN_ERROR";
			}
		return string;
	}

}
