/* 
 * Copyright 2014 Jacopo Aliprandi, Dario Archetti
 * 
 * This file is part of SPF.
 * 
 * SPF is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * SPF is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with SPF.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package it.polimi.dei.spf.framework.local;

import android.os.Parcel;
import android.os.Parcelable;

// TODO: Auto-generated Javadoc
/**
 * The Class ServiceDescriptor.
 */
public class ServiceDescriptor implements Parcelable {

	/** The m service name. */
	private String mServiceName;
	
	/** The m app identifier. */
	private String mAppIdentifier;
	
	/** The m version. */
	private String mVersion;
	
	/** The m intent. */
	private String mIntent;
	
	/** The m consumed verbs. */
	private String[] mConsumedVerbs;

	/**
	 * Instantiates a new service descriptor.
	 *
	 * @param svcName the svc name
	 * @param appIdentifier the app identifier
	 * @param version the version
	 * @param intent the intent
	 * @param consumedVerbs the consumed verbs
	 */
	public ServiceDescriptor(String svcName, String appIdentifier, String version, String intent, String[] consumedVerbs) {
		this.mServiceName = svcName;
		this.mAppIdentifier = appIdentifier;
		this.mVersion = version;
		this.mIntent = intent;
		this.mConsumedVerbs = consumedVerbs;
	}

	/**
	 * Instantiates a new service descriptor.
	 *
	 * @param source the source
	 */
	private ServiceDescriptor(Parcel source) {
		this.mServiceName = source.readString();
		this.mAppIdentifier = source.readString();
		this.mVersion = source.readString();
		this.mIntent = source.readString();
		this.mConsumedVerbs = source.createStringArray();
	}

	/**
	 * Gets the service name.
	 *
	 * @return the service name
	 */
	public String getServiceName() {
		return mServiceName;
	}

	/**
	 * Gets the app identifier.
	 *
	 * @return the app identifier
	 */
	public String getAppIdentifier() {
		return mAppIdentifier;
	}

	/**
	 * Gets the version.
	 *
	 * @return the version
	 */
	public String getVersion() {
		return mVersion;
	}

	/**
	 * Gets the intent.
	 *
	 * @return the intent
	 */
	public String getIntent() {
		return mIntent;
	}
	
	/**
	 * Gets the consumed verbs.
	 *
	 * @return the consumed verbs
	 */
	public String[] getConsumedVerbs(){
		return mConsumedVerbs;
	}
	
	/* (non-Javadoc)
	 * @see android.os.Parcelable#describeContents()
	 */
	@Override
	public int describeContents() {
		return 0;
	}

	/* (non-Javadoc)
	 * @see android.os.Parcelable#writeToParcel(android.os.Parcel, int)
	 */
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(mServiceName);
		dest.writeString(mAppIdentifier);
		dest.writeString(mVersion);
		dest.writeString(mIntent);
		dest.writeStringArray(mConsumedVerbs);
	}

	/** The Constant CREATOR. */
	public final static Parcelable.Creator<ServiceDescriptor> CREATOR = new Parcelable.Creator<ServiceDescriptor>() {

		@Override
		public ServiceDescriptor createFromParcel(Parcel source) {
			return new ServiceDescriptor(source);
		}

		@Override
		public ServiceDescriptor[] newArray(int size) {
			return new ServiceDescriptor[size];
		}

	};
}