/* 
 * Copyright 2014 Jacopo Aliprandi, Dario Archetti
 * 
 * This file is part of SPF.
 * 
 * SPF is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * SPF is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with SPF.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package it.polimi.dei.spf.lib.async.search;

import it.polimi.dei.spf.framework.local.SearchDescriptor;

/**
 * Interface for components which provide low-level access to search
 * functionalities.
 * 
 * @hide
 * @author darioarchetti
 * 
 */
public interface SearchInterface {

	/**
	 * Starts a search
	 * 
	 * @param searchDescriptor
	 *            - the descriptor of the search
	 * @param callback
	 *            - the callback to notify the caller that the search has
	 *            started.
	 */
	public void startSearch(SearchDescriptor searchDescriptor, SearchStartedCallback callback);

	/**
	 * Stops a search previously registerd
	 * 
	 * @param queryId
	 *            - the id of the query
	 */
	public void stopSearch(String queryId);

	/**
	 * Look for a SPF remote instance. Returns true if the istance is reachable
	 * (i.e. it is in proximity), otherwise false.
	 * 
	 * @param the
	 *            identifier of the instance to look for
	 * @return true if the instance is reachable
	 */
	public boolean lookup(String identifier);

	/**
	 * 
	 * Interface to notify the caller of
	 * {@link SearchInterface#startSearch(SearchDescriptor, SearchStartedCallback)}
	 * that the search has started and to provide it the query id.
	 * 
	 * @hide
	 * @author darioarchetti
	 * 
	 */
	public interface SearchStartedCallback {
		/**
		 * Notifies tht the query has started.
		 * 
		 * @param queryId
		 *            - the id of the started query.
		 */
		public void run(String queryId);
	}
}
