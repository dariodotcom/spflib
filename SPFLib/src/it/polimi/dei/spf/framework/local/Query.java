/* 
 * Copyright 2014 Jacopo Aliprandi, Dario Archetti
 * 
 * This file is part of SPF.
 * 
 * SPF is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * SPF is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with SPF.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package it.polimi.dei.spf.framework.local;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

// TODO: Auto-generated Javadoc
/**
 * The Class Query.
 *
 * @author darioarchetti
 */
public class Query implements Parcelable {

	/** The m tags. */
	private List<String> mTags;
	
	/** The m profile fields. */
	private Map<String, String> mProfileFields;
	
	/** The m apps. */
	private List<String> mApps;

	/**
	 * Instantiates a new query.
	 *
	 * @param in the in
	 */
	@SuppressWarnings("unchecked")
	private Query(Parcel in) {
		ClassLoader cl = getClass().getClassLoader();
		mTags = in.readArrayList(cl);
		mProfileFields = new HashMap<String, String>();
		in.readMap(mProfileFields, cl);
		mApps = in.readArrayList(cl);
	}

	/**
	 * Instantiates a new query.
	 */
	private Query() {
		mTags = new ArrayList<String>();
		mProfileFields = new HashMap<String, String>();
		mApps = new ArrayList<String>();
	}

	/**
	 * Or.
	 *
	 * @param q the q
	 * @return the query
	 */
	public Query or(Query q) {
		return null;// TODO ?
	}

	/**
	 * Gets the tags.
	 *
	 * @return the tags
	 */
	public List<String> getTags() {
		return mTags;
	}

	/**
	 * Gets the profile fields.
	 *
	 * @return the profile fields
	 */
	public Map<String, String> getProfileFields() {
		return mProfileFields;
	}

	/**
	 * Gets the apps.
	 *
	 * @return the apps
	 */
	public List<String> getApps() {
		return mApps;
	}

	/**
	 * The Class Builder.
	 *
	 * @author darioarchetti
	 */
	public static class Builder {

		/** The m query. */
		private Query mQuery;

		/**
		 * Instantiates a new builder.
		 */
		public Builder() {
			mQuery = new Query();
		}

		/**
		 * Sets the profile field.
		 *
		 * @param <E> the element type
		 * @param profileField the profile field
		 * @param value the value
		 * @return the builder
		 */
		public <E> Builder setProfileField(ProfileField<E> profileField, E value) {
			checkQuery();
			mQuery.mProfileFields.put(profileField.getIdentifier(), ProfileFieldConverter.forField(profileField).toStorageString(value));
			return this;
		}

		/**
		 * Sets the tag.
		 *
		 * @param tag the tag
		 * @return the builder
		 */
		public Builder setTag(String tag) {
			checkQuery();
			mQuery.mTags.add(tag);
			return this;
		}

		/**
		 * Sets the tag list.
		 *
		 * @param tagList the tag list
		 * @return the builder
		 */
		public Builder setTagList(List<String> tagList) {
			checkQuery();
			mQuery.mTags.addAll(tagList);
			return this;
		}

		/**
		 * Sets the app identifier.
		 *
		 * @param appIdentifier the app identifier
		 * @return the builder
		 */
		public Builder setAppIdentifier(String appIdentifier) {
			checkQuery();
			mQuery.mApps.add(appIdentifier);
			return this;
		}

		/**
		 * Builds the.
		 *
		 * @return the query
		 */
		public Query build() {
			Query q = mQuery;
			mQuery = null;
			return q;
		}

		/**
		 * Check query.
		 */
		private void checkQuery() {
			if (mQuery == null) {
				throw new IllegalStateException("Query already built");
			}
		}
	}

	/** The Constant TAGS. */
	private static final String TAGS = "tags";
	
	/** The Constant FIELDS. */
	private static final String FIELDS = "fields";
	
	/** The Constant APPS. */
	private static final String APPS = "apps";
	
	/** The Constant TAG. */
	private static final String TAG = "Query";

	/** The m query string. */
	private String mQueryString;

	/**
	 * To query string.
	 *
	 * @return the string
	 */
	public String toQueryString() {
		if (mQueryString != null) {
			return mQueryString;
		}

		JSONObject queryString = new JSONObject();

		JSONArray tags = new JSONArray();
		for (String t : mTags) {
			tags.put(t);
		}

		JSONArray fields = new JSONArray();
		for (String k : mProfileFields.keySet()) {
			fields.put(k + "," + mProfileFields.get(k));
		}

		JSONArray apps = new JSONArray();
		for (String a : mApps) {
			apps.put(a);
		}

		try {
			queryString.put(TAGS, tags);
			queryString.put(FIELDS, fields);
			queryString.put(APPS, apps);
		} catch (JSONException e) {
			// Should never happen
			Log.e(TAG, "Error marshalling query:", e);
		}

		return queryString.toString();
	}

	/**
	 * From query string.
	 *
	 * @param input the input
	 * @return the query
	 */
	public static Query fromQueryString(String input) {
		Query q = new Query();

		try {
			JSONObject o = new JSONObject(input);

			JSONArray tags = o.getJSONArray(TAGS);
			for (int i = 0; i < tags.length(); i++) {
				q.mTags.add(tags.getString(i));
			}

			JSONArray fields = o.getJSONArray(FIELDS);
			for (int i = 0; i < fields.length(); i++) {
				String[] parts = fields.getString(i).split(",");
				q.mProfileFields.put(parts[0], parts[1]);
			}

			JSONArray apps = o.getJSONArray(APPS);
			for (int i = 0; i < apps.length(); i++) {
				q.mApps.add(apps.getString(i));
			}

			q.mQueryString = input;
		} catch (JSONException e) {
			Log.e(TAG, "JSON exception unmarshalling query", e);
		}

		return q;
	}

	/* (non-Javadoc)
	 * @see android.os.Parcelable#describeContents()
	 */
	@Override
	public int describeContents() {
		return 0;
	}

	/* (non-Javadoc)
	 * @see android.os.Parcelable#writeToParcel(android.os.Parcel, int)
	 */
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeList(mTags);
		dest.writeMap(mProfileFields);
		dest.writeList(mApps);
	}

	/** The Constant CREATOR. */
	public static final Parcelable.Creator<Query> CREATOR = new Parcelable.Creator<Query>() {

		@Override
		public Query createFromParcel(Parcel source) {
			return new Query(source);
		}

		@Override
		public Query[] newArray(int size) {
			return new Query[size];
		}
	};
}
