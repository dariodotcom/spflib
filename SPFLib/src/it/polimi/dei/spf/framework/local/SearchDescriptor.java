/* 
 * Copyright 2014 Jacopo Aliprandi, Dario Archetti
 * 
 * This file is part of SPF.
 * 
 * SPF is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * SPF is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with SPF.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package it.polimi.dei.spf.framework.local;

import android.os.Parcel;
import android.os.Parcelable;

// TODO: Auto-generated Javadoc
/**
 * 
 * Describe the configuration of a search operation. Its use is intended to ease
 * the communication beetween the application and the spf framework. It defines
 * the query, the duration of the search and the frequency of the search signal.
 */
/*
 * TODO Use it directly in the SPFLib ...different from QueryInfo which is an
 * internal data structure! (To favor decoupling )
 */
public final class SearchDescriptor implements Parcelable {

	/** The m interval btw signals. */
	private long mIntervalBtwSignals;
	
	/** The m number of signals. */
	private int mNumberOfSignals;
	
	/** The m query. */
	private Query mQuery;

	/**
	 * Instantiates a new search descriptor.
	 *
	 * @param intervalBtwSignals the interval btw signals
	 * @param number_of_signals the number_of_signals
	 * @param query the query
	 */
	public SearchDescriptor(long intervalBtwSignals, int number_of_signals, Query query) {
		super();
		this.mIntervalBtwSignals = intervalBtwSignals;
		this.mNumberOfSignals = number_of_signals;
		this.mQuery = query;
	}

	/**
	 * Gets the query.
	 *
	 * @return the query string
	 */
	public Query getQuery() {
		return mQuery;
	}

	/**
	 * Gets the interval btw signals.
	 *
	 * @return the intervalBtwSignals
	 */
	public long getIntervalBtwSignals() {
		return mIntervalBtwSignals;
	}

	/**
	 * Gets the number of signals.
	 *
	 * @return the numberOfSignals
	 */
	public int getNumberOfSignals() {
		return mNumberOfSignals;
	}

	/*
	 * Set of methods an constructor required to implement the Parcelable
	 * interface and the aidl.
	 */

	/**
	 * Instantiates a new search descriptor.
	 *
	 * @param source the source
	 */
	private SearchDescriptor(Parcel source) {
		this.mIntervalBtwSignals = source.readLong();
		this.mNumberOfSignals = source.readInt();
		this.mQuery = source.readParcelable(getClass().getClassLoader());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.os.Parcelable#describeContents()
	 */
	@Override
	public int describeContents() {
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.os.Parcelable#writeToParcel(android.os.Parcel, int)
	 */
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeLong(mIntervalBtwSignals);
		dest.writeInt(mNumberOfSignals);
		dest.writeParcelable(mQuery, 0);

	}

	/** The Constant CREATOR. */
	public final static android.os.Parcelable.Creator<SearchDescriptor> CREATOR = new Creator<SearchDescriptor>() {

		@Override
		public SearchDescriptor[] newArray(int size) {

			return new SearchDescriptor[size];
		}

		@Override
		public SearchDescriptor createFromParcel(Parcel source) {
			return new SearchDescriptor(source);
		}
	};

}
