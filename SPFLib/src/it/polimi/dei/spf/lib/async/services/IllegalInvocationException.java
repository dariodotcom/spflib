/* 
 * Copyright 2014 Jacopo Aliprandi, Dario Archetti
 * 
 * This file is part of SPF.
 * 
 * SPF is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * SPF is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with SPF.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package it.polimi.dei.spf.lib.async.services;

/**
 * Indicates that the invocation of a service has failed due error in the
 * configuration, for example parameters mismatch.
 */
public class IllegalInvocationException extends Exception {

	private static final long serialVersionUID = -4848082620238254886L;

	public IllegalInvocationException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
	}

	public IllegalInvocationException(Throwable throwable) {
		super(throwable);
	}

	public IllegalInvocationException() {
		super();
	}

	public IllegalInvocationException(String detailMessage) {
		super(detailMessage);
	}

}