/* 
 * Copyright 2014 Jacopo Aliprandi, Dario Archetti
 * 
 * This file is part of SPF.
 * 
 * SPF is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * SPF is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with SPF.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package it.polimi.dei.spf.lib.async.profile;

import it.polimi.dei.spf.framework.local.ProfileField;
import it.polimi.dei.spf.framework.local.ProfileFieldContainer;
import it.polimi.dei.spf.framework.local.SPFError;

/**
 * Interface for a component that allows low level access to remote profiles.
 * 
 * @author darioarchetti
 * 
 */
public interface RemoteProfileInterface {



	/**
	 * Obtains a bulk of profile fields from a remote profile. The bulk is
	 * retrieved as an instance of {@link ProfileFieldContainer} from which the
	 * single field can be retrieved.
	 * 
	 * @param identifier
	 *            - the identifier of the person from which to retrieve the
	 *            fields
	 * @param fields
	 *            - the list of {@link ProfileField} to retrieve from the remote
	 *            profile
	 * @return a container that holds the values of requested fields
	 */
	public ProfileFieldContainer getProfileBulk(String identifier, String[] fields, SPFError err);
}
